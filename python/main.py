import json
import socket
import logging
import sys
import math

class NoobBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        ## Initialize the logger
        self.logger = logging.getLogger("superman")
        self.logger.setLevel(logging.DEBUG)
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)
        formatter = logging.Formatter("%(levelname)s - %(message)s")
        ch.setFormatter(formatter)
        self.logger.addHandler(ch)
        self.throttle = 1.
        self.pieceIndex = 0
        self.pieceDistance = 0
        self.speed = 0

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def set_throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        self.logger.debug("Joined")
        self.ping()

    def on_game_init(self, data):
        self.logger.debug("Game Init")
        jsonObj = data
        race = jsonObj["race"]
        track = race["track"]
        self.pieces = track["pieces"]
        log = ""
        i = 0
        for piece in self.pieces:
            log = log + "piece " + str(i) + " "
            if "length" in piece:
                log = log + str(piece["length"]) + " "
            if "switch" in piece:
                log = log + str(piece["switch"]) + " "
            if "radius" in piece:
                log = log + str(piece["radius"]) + " "
            if "angle" in piece:
                log = log + str(piece["angle"])
            log = log + "\n"
            i += 1

        self.logger.info(log)
        self.ping()

    def on_game_start(self, data):
        self.logger.debug("Race started")
        self.ping()

    def on_car_positions(self, data):
        # Parse the data to create a csv
        # decoded_data = json.dumps(data) # Json format        
        log = ""
        jsonObj = data[0] # TODO Check if there are more than one data 
        piecePosition = jsonObj["piecePosition"]
        angle = jsonObj["angle"]
        if self.pieceIndex == piecePosition["pieceIndex"]:
            self.speed = piecePosition["inPieceDistance"] - self.pieceDistance
        else:
            self.speed = 100 - self.pieceDistance + piecePosition["inPieceDistance"]

        self.pieceIndex = piecePosition["pieceIndex"]
        self.pieceDistance = piecePosition["inPieceDistance"]
        log = log + str(piecePosition["pieceIndex"]) + " " + str(self.pieceDistance) + " " + str(angle)
        #self.logger.info(log)
           
        currentPiece = self.pieces[self.pieceIndex]
        
        nextPieceIndex = self.pieceIndex + 1 
        if nextPieceIndex > len(self.pieces) - 1:
            nextPieceIndex = 0
        nextPiece = self.pieces[nextPieceIndex]

        nextNextPieceIndex = nextPieceIndex + 1 
        if nextNextPieceIndex > len(self.pieces) - 1:
            nextNextPieceIndex = 0
        nextNextPiece = self.pieces[nextNextPieceIndex]

        G = 9

        # we're alreay on curve
        if "angle" in currentPiece and "angle" in nextPiece:
            currentAngle = currentPiece["angle"] * 3.14 /180.
            tanalpha = abs(math.tan(currentAngle))
            currentRadius = currentPiece["radius"]
            vmax = math.sqrt( tanalpha * 4.95 * G)
            print "tata"
            print str(vmax) + " " + str(self.speed)
            if self.speed > vmax:
                self.throttle /= 3
            elif self.speed < vmax:
                self.throttle += .2


        # we're on curve but next piece is straight
        elif "angle" not in nextPiece and "angle" in currentPiece:
            totalCurve = currentPiece["angle"] / 360. * 3.14 * currentPiece["radius"]
            remainingPiece = totalCurve -  self.pieceDistance 
            currentAngle = currentPiece["angle"] * 3.14 /180.
            tanalpha = abs(math.tan(currentAngle))
            currentRadius = currentPiece["radius"]
            vmax = math.sqrt( tanalpha * 4.95 * G);
            print "toto"
            print str(vmax) + " " + str(self.speed)
            if self.speed > vmax:
                self.throttle /= 3
            elif self.speed < vmax:
                self.throttle += .2
            if remainingPiece < 50:
                self.throttle = 1.

        # we're on straight piece but next has curve
        elif "angle" in nextPiece:
            remainingPiece = currentPiece["length"] - self.pieceDistance
            currentAngle = nextPiece["angle"] * 3.14 /180.
            tanalpha = abs(math.tan(currentAngle))
            currentRadius = nextPiece["radius"]
            vmax = math.sqrt( tanalpha * 4.95 * G);
            print "titi"
            print str(vmax) + " " + str(self.speed)
            if remainingPiece < 75.:
                if self.speed > vmax:
                    self.throttle /= 5
                elif self.speed < vmax:
                    self.throttle += .2
           
        elif self.speed > 7 and "angle" in nextNextPiece:
                self.throttle *= .8 
                if self.throttle < .5:
                    self.throttle = .5

        # we're on straight piece
        else:
            self.throttle = 1.

        if self.throttle > 1.:
            self.throttle = 1.
        elif self.throttle < 0.35:
            self.throttle = .35

        log += " " + str(self.throttle)+" "+str(self.speed)
        self.logger.info(log)
        
        self.set_throttle(self.throttle)

    def on_crash(self, data):
        self.logger.critical("Someone crashed")
        self.logger.critical( str(self.pieceIndex) + " " + str(self.pieces[self.pieceIndex]))
        self.ping()
        sys.exit(0)

    def on_game_end(self, data):
        self.logger.debug("Race ended")
        self.ping()

    def on_error(self, data):
        self.logger.debug("Error: {0}".format(data))
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameInit': self.on_game_init,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                self.logger.debug("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
